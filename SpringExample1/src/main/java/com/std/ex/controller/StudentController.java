package com.std.ex.controller;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.std.ex.entity.Student;
import com.std.ex.service.StudentService;
import com.sun.xml.bind.v2.model.core.ID;

@RestController
@RequestMapping("/api/student")
public class StudentController {


	private StudentService service;
	
	public StudentController(StudentService service) {
		this.service=service;
	}


	@PostMapping("/students/add")
	public ResponseEntity<String> save(@RequestBody Student student){
         boolean save=service.Save(student);
         if(save) {
        	 return new ResponseEntity<String>("Inserted",HttpStatus.OK);
         }
         return new ResponseEntity<String>("insertion failure",HttpStatus.INTERNAL_SERVER_ERROR);
		
	}
	
	@GetMapping("/students/findAllById/{id}")
	public ResponseEntity<List<Student>> findAllById(@PathVariable Iterable<Serializable> id){
		List<Student> find=service.findAllByID(id);
		return new ResponseEntity<List<Student>>(find,HttpStatus.OK);
	}
	@GetMapping("/students/{id}")
	public ResponseEntity<Student> findByID(@PathVariable(value = "id") Integer id){
         Student findbyId=service.findByID(id);

         return new ResponseEntity<Student>(findbyId,HttpStatus.OK);
		
	}
	@GetMapping
	public ResponseEntity<List<Student>> findAll(){
         List<Student> findAll=service.findAll();

         return new ResponseEntity<List<Student>>(findAll,HttpStatus.OK);
		
	}
	@DeleteMapping("/student{id}/delete")
	public ResponseEntity<String> deleteById(@PathVariable(value = "id") Integer id){
         boolean findbyId=service.deleteByID(id);
         if(findbyId) {
        	 return new ResponseEntity<String>("Deleted successful",HttpStatus.OK);
         }

         return new ResponseEntity<String>("deletion failure",HttpStatus.EXPECTATION_FAILED);
		
	}
	
}
