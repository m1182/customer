/**
 * 
 */
package com.std.ex.service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.std.ex.entity.Student;
import com.std.ex.repositery.StudentRepositery;
import com.sun.xml.bind.v2.model.core.ID;

/**
 * @author hp
 *
 */
@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepositery repo;
	
	@Override
	public boolean Save(Student std) {
		Student save=repo.save(std);
		return save.getID() !=null;

	}

	@Override
	public List<Student> findAllByID(Iterable<Serializable> id) {
		List<Student> find=repo.findAllById(id);
		return  find;
	}

	@Override
	public List<Student> findAll() {
		// TODO Auto-generated method stub
		return repo.findAll();
	}
	

	@Override
	public boolean deleteByID(Integer id) {
		try {
			repo.deleteById(id);
			return true;
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Student findByID(Integer id) {
		Optional<Student>find =repo.findById(id);
		if(find.isPresent()) {
			return find.get();
		}
		return null;
	}

}
