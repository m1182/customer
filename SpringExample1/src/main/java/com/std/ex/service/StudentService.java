package com.std.ex.service;

import java.io.Serializable;
import java.util.List;

import com.std.ex.entity.Student;

public interface StudentService {

	boolean Save(Student std);
	
	Student findByID(Integer id);
	
	List<Student> findAll();
	
	boolean deleteByID(Integer id);

	List<Student> findAllByID(Iterable<Serializable> id);
}
