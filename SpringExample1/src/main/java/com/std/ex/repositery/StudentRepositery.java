package com.std.ex.repositery;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.std.ex.entity.Student;

@Repository
public interface StudentRepositery extends JpaRepository<Student, Serializable> {

}
