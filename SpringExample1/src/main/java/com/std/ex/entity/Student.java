package com.std.ex.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "student")
public class Student {

	@Id
	@Column(name = "ID")
	@GeneratedValue
	private Integer ID;
	
	@Column(name = "studentName")
	private String studentName;
	
	@Column(name="Age")
	private Integer Age;
	
	@Column(name = "stdClass")
	private String stdClass;

	

	public Integer getID() {
		return ID;
	}



	public void setID(Integer iD) {
		ID = iD;
	}



	public String getStudentName() {
		return studentName;
	}



	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}



	public Integer getAge() {
		return Age;
	}



	public void setAge(Integer age) {
		Age = age;
	}



	public String getstdClass() {
		return stdClass;
	}



	public void setstdClass(String class1) {
		stdClass = class1;
	}



	@Override
	public String toString() {
		return "Student [ID=" + ID + ", studentName=" + studentName + ", Age=" + Age + ", Class=" + stdClass + "]";
	}
	
	
	
}
